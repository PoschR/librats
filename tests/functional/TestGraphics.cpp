//
// Created by robert on 06.03.17.
//

#include <memory>

#include <GL/glew.h>
#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <SFML/Graphics.hpp>

#include <Game/Graphics/VertexLayout.h>
#include <Game/Graphics/Shaders/FileShader.h>
#include <Game/Graphics/Model.h>
#include <Game/Graphics/Camera.h>
#include <Game/Graphics/Visualization.h>


struct Vertex{
    glm::vec3 position;
    glm::vec3 normal;
};

void move_camera_up_down(rats::Camera &camera, float& dir, sf::Clock &game_clock);

int main(int argc, char** argv)
{
    sf::ContextSettings opengl_settings;

    opengl_settings.depthBits = 24;
    opengl_settings.stencilBits = 8;
    opengl_settings.antialiasingLevel = 4;
    opengl_settings.majorVersion = 3;
    opengl_settings.minorVersion = 3;

    sf::RenderWindow window(sf::VideoMode(800, 600), "Test Graphics", sf::Style::Close | sf::Style::Titlebar, opengl_settings);

    rats::Visualization opengl_visualization;
    rats::Camera camera(rats::Position(0,0, 2.0f),
                        rats::LookAt(0,0,0),
                        rats::Direction(0, 1, 0),
                        rats::Frustum(rats::FoV(45.0f),
                                      rats::Aspect(4.0f/3.0f),
                                      0.1f, 100.0f));

    rats::VertexLayout layout = {
            VERTEX_ATTRIBUTE(Vertex, position, GL_FLOAT, "position", 3),
            VERTEX_ATTRIBUTE(Vertex, normal, GL_FLOAT, "normal", 3)
    };
    rats::FileShader bare_shader("shader/perspective.vert", "shader/bare.frag");

    rats::GameObject sphere1;
    rats::GameObject sphere2(glm::translate(glm::mat4(), rats::Translation(0.0f, 1.0f, 0.0f)));

    auto sphere_comp  = rats::make_ico_sphere_model<Vertex>(0.5f, 2, layout);
    auto sphere2_comp = rats::make_ico_sphere_model<Vertex>(0.5f, 5, layout);

    sphere1.addComponent(sphere_comp);
    sphere2.addComponent(sphere2_comp);

    opengl_visualization.add(sphere_comp, bare_shader);
    opengl_visualization.add(sphere2_comp, bare_shader);;

    float dir = 1.0f;

    sf::Clock game_clock;
    while(window.isOpen())
    {
        sf::Event event;
        while(window.pollEvent(event)){
            if(event.type == sf::Event::Closed)
                window.close();
        }

        sphere1.update();
        sphere2.update();

        opengl_visualization.draw(camera);

        //display all data
        window.display();

        //move camera up and down
        move_camera_up_down(camera, dir, game_clock);
    }

    return 0;
}

void move_camera_up_down(rats::Camera &camera, float& dir, sf::Clock &game_clock)
{
    const rats::Position& cam_pos = camera.position();
    const float y_thres = 1.0f;

    if(cam_pos.y > y_thres)
            dir = -1.0f;
        else if(cam_pos.y < -y_thres)
            dir = 1.0f;

    auto elapsed_time = game_clock.getElapsedTime();
    game_clock.restart();

    camera.translate(rats::Translation(0, dir * elapsed_time.asSeconds(), 0));
}

