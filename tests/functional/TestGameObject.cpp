//
// Created by robert on 25.07.17.
//

#include <iostream>

#include <GL/glew.h>
#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <SFML/Graphics.hpp>

#include <Game/Graphics/VertexLayout.h>
#include <Game/Graphics/Shaders/FileShader.h>
#include <Game/Graphics/Model.h>
#include <Game/Graphics/Camera.h>
#include <Game/Graphics/Visualization.h>

#include <Game/Physics/Bullet/BulletBody.h>
#include <Game/Physics/Bullet/BulletPhysics.h>


struct Vertex
{
    glm::vec3 position;
    glm::vec3 normal;
};

int main(int argc, char** argv)
{
    sf::ContextSettings opengl_settings;

    opengl_settings.depthBits = 24;
    opengl_settings.stencilBits = 8;
    opengl_settings.antialiasingLevel = 4;
    opengl_settings.majorVersion = 3;
    opengl_settings.minorVersion = 3;

    sf::RenderWindow window(sf::VideoMode(800, 600), "Test GameObject", sf::Style::Close | sf::Style::Titlebar, opengl_settings);

    rats::Visualization opengl_visualization;
    rats::Camera camera(rats::Position(0,2.0f, 2.0f),
                        rats::LookAt(0,0,0),
                        rats::Direction(0, 1, 0),
                        rats::Frustum(rats::FoV(45.0f),
                                      rats::Aspect(4.0f/3.0f),
                                      0.1f, 100.0f));

    rats::VertexLayout layout = {
            VERTEX_ATTRIBUTE(Vertex, position, GL_FLOAT, "position", 3),
            VERTEX_ATTRIBUTE(Vertex, normal, GL_FLOAT, "normal", 3)
    };
    rats::FileShader bare_shader("shader/perspective.vert", "shader/bare.frag");

    rats::BulletPhysics rb_system;
    rb_system.changeGravity(rats::Force(0.f, -9.81f, 0.f));

    rats::IBody& sphere_body = rb_system.add(rats::Shape::SPHERE, {0.5f}, rats::Mass(100.0f), rats::Position(0.0f, 10.0f, 0.0f));
    rats::IBody& floor_body = rb_system.add(rats::Shape::PLANE, {0.0f, 1.0f, 0.0f, 0.0f}, rats::Mass(0.0f), rats::Position(0.0f, 0.0f, 0.0f));

    rats::GameObject sphere1;
    rats::GameObject floor = {&floor_body};

    auto sphere_comp  = rats::make_ico_sphere_model<Vertex>(0.5f, 2, layout);

    sphere1.addComponent(sphere_body);
    sphere1.addComponent(sphere_comp);
    opengl_visualization.add(sphere_comp, bare_shader);

    sf::Clock game_clock;
    while(window.isOpen())
    {
        sf::Event event;
        while(window.pollEvent(event)){
            if(event.type == sf::Event::Closed)
                window.close();
        }

        float dt = game_clock.getElapsedTime().asSeconds();
        game_clock.restart();

        rb_system.step(dt);

        sphere1.update();
        floor.update();

        opengl_visualization.draw(camera);

        //display all data
        window.display();
    }

    return 0;
}