//
// Created by robert on 13.02.17.
//

#include <catch.hpp>

#include <Game/Component.h>
#include <Game/GameObject.h>

using namespace rats;


TEST_CASE("GameObject IComponent management", "[GameObject]")
{
    class MockComponent : public IComponent
    {
    public:
        bool called = false;

        void update(GameObject &gameObject)  {
            called = true;
        }

    };

    SECTION("Standard constructor")
    {
        GameObject testObject;
        MockComponent* component1 = new MockComponent();
        MockComponent* component2 = new MockComponent();
        MockComponent* component3 = new MockComponent();

        testObject.addComponent(*component1);
        testObject.addComponent(*component2);
        testObject.addComponent(*component3);

        testObject.update();

        REQUIRE(component1->called);
        REQUIRE(component2->called);
        REQUIRE(component3->called);
    }
}