//
// Created by robert on 10.02.17.
//

#include <catch.hpp>

#include "Game/Physics/Bullet/BulletPhysics.h"

using namespace rats;


const float FPS = 60.f;
const float TIME_STEP = 1.f / FPS;

TEST_CASE("Testing BulletPhysics", "[BulletPhysics]")
{
    BulletPhysics physics;
    physics.changeGravity(Force{0.f, -9.81f, 0.f});

    IBody& body = physics.add(Shape::SPHERE, {1.f}, Mass(1.f), Position{0.f, 10.f, 0.f});

    SECTION("creating a body")
    {
        auto pos = body.position();

        REQUIRE(pos[0] == 0.f);
        REQUIRE(pos[1] == 10.f);
        REQUIRE(pos[2] == 0.f);
    }

    SECTION("one step of gravity")
    {
        physics.step(TIME_STEP);

        auto pos = body.position();

        REQUIRE(pos[1] < 10.f);
    }
}