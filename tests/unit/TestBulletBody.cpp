//
// Created by robert on 10.02.17.
//

#define CATCH_CONFIG_MAIN

#include <catch.hpp>

#include <Game/Physics/Bullet/BulletBody.h>

using namespace rats;

TEST_CASE("testing constructor of BulletBody", "[BulletBody]")
{
    BulletBody body(Shape::SPHERE, {1.f}, Mass(1.f), Position{0.f, 1.f, 2.f});

    SECTION("get position from Bullet")
    {
        auto pos = body.position();

        REQUIRE(pos[0] == 0.f);
        REQUIRE(pos[1] == 1.f);
        REQUIRE(pos[2] == 2.f);
    }

    SECTION("get velocity from Bullet")
    {
        auto vel = body.velocity();

        REQUIRE(vel[0] == 0.f);
        REQUIRE(vel[1] == 0.f);
        REQUIRE(vel[2] == 0.f);
    }

    SECTION("move the body by a small offset")
    {
        body.move(Translation{1.0f, 0.0f, 0.0f});
        auto pos = body.position();

        REQUIRE(pos[0] == 1.f);
    }

    SECTION("move the body to a specific point")
    {
        body.moveTo(Position{-10.f, 3.f, -0.5f});
        auto pos = body.position();

        REQUIRE(pos[0] == -10.f);
        REQUIRE(pos[1] ==   3.f);
        REQUIRE(pos[2] ==  -0.5f);
    }
}