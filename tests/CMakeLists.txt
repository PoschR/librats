include_directories(/home/robert/Libraries/Catch/)

set(TEST_SOURCES unit/TestBulletBody.cpp unit/TestBulletPhysics.cpp unit/TestGameObject.cpp)

add_executable(RatsTest ${TEST_SOURCES})
target_link_libraries(RatsTest librats)

enable_testing()

add_subdirectory(functional)