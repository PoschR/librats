//
// Created by robert on 28.11.16.
//

#ifndef LIBRATS_GAMEOBJECT_H
#define LIBRATS_GAMEOBJECT_H

#include <list>
#include <memory>

#include <glm/mat4x4.hpp>


namespace rats
{
    class IComponent;

    using ComponentPointer = std::shared_ptr<IComponent>;

    /**
     *
     */
    class GameObject
    {
    public:

        GameObject();

        GameObject(const glm::mat4& transform);

        GameObject(std::initializer_list<IComponent*> components);

        GameObject(const std::list<IComponent*>& components, const glm::mat4& transform);

        virtual ~GameObject();

        void addComponent(IComponent& component);

        void update();

    private:
        std::list<IComponent*> components_;
    public:
        glm::mat4 transform_;
    };
}

#endif //LIBRATS_GAMEOBJECT_H
