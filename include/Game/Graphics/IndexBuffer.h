//
// Created by robert on 07.03.17.
//

#ifndef LIBRATS_INDEXBUFFER_H
#define LIBRATS_INDEXBUFFER_H

#include <GL/glew.h>
#include <vector>
#include <memory>

#include "GraphicsBuffer.h"
#include "VertexBuffer.h"

namespace rats
{
    using Index = unsigned int;
    using Indices = std::vector<Index>;

    template <class VertexSpecs>
    using VertexBufferPointer = std::unique_ptr<VertexBuffer<VertexSpecs>>;

    template <class VertexSpecs>
    class IndexBuffer : public IGraphicsBuffer
    {
    public:
        IndexBuffer(BufferType type, VertexBufferPointer<VertexSpecs> vertex_buffer, const Indices& indices);
        virtual ~IndexBuffer();


        void bind();
        unsigned int size() const;

    private:
        BufferType type_;
        Indices indices_;
        GLuint index_buffer_;
        VertexBufferPointer<VertexSpecs> vertex_buffer_;

    };
}

#include "IndexBuffer.inl"

#endif
