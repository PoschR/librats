//
// Created by robert on 13.02.17.
//

#ifndef LIBRATS_VISUAL_H
#define LIBRATS_VISUAL_H

#include <Game/Component.h>

namespace rats {

    class FileShader;

    using Transform = glm::mat4;

    /**
     * Component interface used by the visualization. Every class that inherits from this represents some visual
     * element that can be drawn.
     */
    class IVisual : public IComponent {
    public:
        virtual ~IVisual() = default;

        /**
         * Inherited update method (implement accordingly)
         * @param gameObject
         */
        virtual void update(GameObject& gameObject) = 0;

        /**
         * Render the visual in this function.
         */
        virtual void draw() = 0;

        virtual const Transform& transform() const = 0;
    };
}




#endif //LIBRATS_VISUAL_H
