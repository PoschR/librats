//
// Created by robert on 14.07.17.
//

#ifndef LIBRATS_VERTEXLAYOUT_H
#define LIBRATS_VERTEXLAYOUT_H

#include <vector>
#include <string>

#include <GL/glew.h>

#define VERTEX_ATTRIBUTE(VERTEX_STRUCT, VARIABLE, TYPE, NAME, NUM_ELEMENTS) rats::VertexAttribute{NAME, offsetof(VERTEX_STRUCT, VARIABLE), NUM_ELEMENTS, sizeof(VERTEX_STRUCT), TYPE}

namespace rats
{
    class VertexAttribute;
    using ConstAttributeIterator = std::vector<VertexAttribute>::const_iterator;

    class VertexAttribute
    {
    public:

        VertexAttribute(const std::string &name, size_t offset, size_t size, size_t stride, GLenum type);

        virtual ~VertexAttribute() = default;

        size_t offset() const;

        size_t size() const;

        size_t stride() const;

        GLenum type() const;

        const std::string& name() const;
    private:
        GLenum type_;
        size_t size_;
        size_t stride_;
        size_t offset_;
        std::string name_;
    };

    class VertexLayout
    {
    public:

        VertexLayout(std::initializer_list<VertexAttribute> vertex_attributes);

        virtual ~VertexLayout() = default;

        void use() const;

        void useAttribute(unsigned int attribute_index) const;

        const ConstAttributeIterator find(const std::string& attribute_name) const;

        const ConstAttributeIterator end() const;

    private:
        std::vector<VertexAttribute> attributes_;
    };
}


#endif //LIBRATS_VERTEXLAYOUT_H
