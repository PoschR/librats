#include <Utilities/utilities.h>

namespace rats
{

    Model::Model(GraphicsBufferPointer graphics_buffer, bool indexed)
    {
        indexed_ = indexed;
        graphics_buffer_ = std::move(graphics_buffer);
    }

    void Model::update(GameObject &game_object) {
        transform_ = game_object.transform_;
    }

    void Model::draw()
    {
        graphics_buffer_->bind();

        if (indexed_)
            glDrawElements(GL_TRIANGLES, graphics_buffer_->size(), GL_UNSIGNED_INT, (void *) 0);
        else
            glDrawArrays(GL_TRIANGLES, 0, graphics_buffer_->size());
    }

    const Transform& Model::transform() const {
        return transform_;
    }

    std::tuple<Vertices, Indices> make_icosahedron()
    {
        float t = (1.f + glm::sqrt(5.f)) / 2.f;
        //icosahedron positions
        Vertices vertices = {
                glm::normalize(glm::vec3(-1.f, t, 0.f)),
                glm::normalize(glm::vec3(1.f, t, 0.f)),
                glm::normalize(glm::vec3(-1.f, -t, 0.f)),
                glm::normalize(glm::vec3(1.f, -t, 0.f)),
                glm::normalize(glm::vec3(0.f, -1.f, t)),
                glm::normalize(glm::vec3(0.f, 1.f, t)),
                glm::normalize(glm::vec3(0.f, -1.f, -t)),
                glm::normalize(glm::vec3(0.f, 1.f, -t)),
                glm::normalize(glm::vec3(t, 0.f, -1.f)),
                glm::normalize(glm::vec3(t, 0.f, 1.f)),
                glm::normalize(glm::vec3(-t, 0.f, -1.f)),
                glm::normalize(glm::vec3(-t, 0.f, 1.f))
        };
        //indices for icosahedron positions
        Indices indices = {
                0, 11, 5,
                0, 5, 1,
                0, 1, 7,
                0, 7, 10,
                0, 10, 11,
                1, 5, 9,
                5, 11, 4,
                11, 10, 2,
                10, 7, 6,
                7, 1, 8,
                3, 9, 4,
                3, 4, 2,
                3, 2, 6,
                3, 6, 8,
                3, 8, 9,
                4, 9, 5,
                2, 4, 11,
                6, 2, 10,
                8, 6, 7,
                9, 8, 1
        };

        return std::make_tuple(vertices, indices);
    }

    std::tuple<Vertices, Indices> make_ico_sphere(const unsigned int refinement)
    {
        Vertices vertices;
        Indices indices;

        std::tie(vertices, indices) = make_icosahedron();

        Indices new_indices;
        std::map<uint64_t, Index> subdivided_indices;

        //compute middle point given two indices
        auto make_middle_point = [&subdivided_indices, &vertices](const Index p1, const Index p2) {
            Index bigger_index = std::max(p1, p2);
            Index smaller_index = std::min(p1, p2);

            uint64_t key = (static_cast<uint64_t >(smaller_index) << 32) + bigger_index;
            auto potential_vertex = subdivided_indices.find(key);
            if (potential_vertex != subdivided_indices.end())
                return potential_vertex->second;

            glm::vec3 &v1 = vertices[bigger_index];
            glm::vec3 &v2 = vertices[smaller_index];

            glm::vec3 middle = glm::normalize(v1 + v2);
            unsigned int index = (unsigned int) vertices.size();

            subdivided_indices.insert({key, index});
            vertices.push_back(middle);

            return index;
        };

        //add triangle indices to new_indices
        auto push_back_triangle = [&new_indices](const unsigned int i0, const unsigned int i1, const unsigned int i2) {
            new_indices.push_back(i0);
            new_indices.push_back(i1);
            new_indices.push_back(i2);
        };

        //refine icosahedron into sphere
        for (int i = 0; i < refinement; i++) {
            new_indices.clear();

            for (unsigned int j = 0; j < indices.size(); j += 3) {
                Index index0 = indices[j];
                Index index1 = indices[j + 1];
                Index index2 = indices[j + 2];

                Index i01 = make_middle_point(index0, index1);
                Index i12 = make_middle_point(index1, index2);
                Index i02 = make_middle_point(index0, index2);

                push_back_triangle(index0, i01, i02);
                push_back_triangle(i01, index1, i12);
                push_back_triangle(i02, i12, index2);
                push_back_triangle(i12, i02, i01);
            }

            indices.clear();
            indices.insert(indices.begin(), new_indices.begin(), new_indices.end());
        }

        return std::make_tuple(vertices, indices);
    }

    template<class VertexSpecs>
    Model make_model_from_data(const Vertices &vertices, const Indices &indices, const Normals &normals,
                               const VertexLayout &layout)
    {
        auto copy_vec3_raw = [] (char* raw_vertex, int offset, const glm::vec3& vector) {
            float* float_vertex = reinterpret_cast<float*>(raw_vertex + offset);

            float_vertex[0] = vector[0];
            float_vertex[1] = vector[1];
            float_vertex[2] = vector[2];
        };

        std::vector<VertexSpecs> final_vertices(vertices.size());

        auto position = layout.find(POSITION_ATTRIBUTE);
        if(position == layout.end())
            throw std::invalid_argument("make_model_from_data: VertexLayout needs a attribute with name \"position\"");

        size_t position_offset = position->offset();

        auto normal = layout.find(NORMAL_ATTRIBUTE);
        int normal_offset = -1;
        if(normal != layout.end())
            normal_offset = (int) normal->offset();

        for (int i=0; i<vertices.size(); i++)
        {
            auto& vertex = final_vertices[i];
            char* raw_vertex = reinterpret_cast<char *>(&vertex);
            int data_index = i;

            copy_vec3_raw(raw_vertex, position_offset, vertices[data_index]);

            if (normal_offset >= 0)
                copy_vec3_raw(raw_vertex, normal_offset, normals[data_index]);
        }

        auto vertex_buffer = make_unique<VertexBuffer<VertexSpecs>>(BufferType::STATIC, layout,
                                                                    final_vertices);
        if(indices.empty())
            return Model(std::move(vertex_buffer), false);

        auto index_buffer = make_unique<IndexBuffer<VertexSpecs>>(BufferType::STATIC, std::move(vertex_buffer), indices);

        return Model(std::move(index_buffer), true);
    }

    template<class VertexSpecs>
    Model make_ico_sphere_model(const float radius, const unsigned int refinement, const VertexLayout &layout)
    {
        Vertices vertices;
        Indices indices;
        std::tie(vertices, indices) = make_ico_sphere(refinement);

        Normals normals = vertices;

        for (auto &vertex : vertices)
            vertex *= radius;

        return make_model_from_data<VertexSpecs>(vertices, indices, normals, layout);
    }

/*template<class VertexSpecs>
Model<VertexSpecs> make_box(const Vec3& half_dims, const glm::mat4& transform, unsigned int position_offset = 0, int normal_offset=-1){

}

template<class VertexSpecs>
Model<VertexSpecs> make_plane(const Vec3& normal, const float plane_constant, glm::mat4 transform, unsigned int position_offset = 0, int normal_offset=-1){

}*/

}




