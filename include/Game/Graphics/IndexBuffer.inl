
namespace rats
{

    template<class VertexSpecs>
    IndexBuffer<VertexSpecs>::IndexBuffer(BufferType type, VertexBufferPointer<VertexSpecs> vertex_buffer, const Indices &indices) :
    type_(type),
    indices_(indices)
    {
            vertex_buffer_ = std::move(vertex_buffer);
            vertex_buffer_->bind();

            glGenBuffers(1, &index_buffer_);
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, index_buffer_);
            if (type_ == BufferType::STATIC)
                glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices_.size() * sizeof(unsigned int), indices_.data(), GL_STATIC_DRAW);
            else
                glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices_.size() * sizeof(unsigned int), indices_.data(), GL_DYNAMIC_DRAW);
    }

    template<class VertexSpecs>
    IndexBuffer<VertexSpecs>::~IndexBuffer() {
        glDeleteBuffers(1, &index_buffer_);
    }


    template<class VertexSpecs>
    void IndexBuffer<VertexSpecs>::bind() {
        vertex_buffer_->bind();
    }


    template<class VertexSpecs>
    unsigned int IndexBuffer<VertexSpecs>::size() const {
        return (unsigned int) indices_.size();
    }

}