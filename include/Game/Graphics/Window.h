//
// Created by robert on 17.02.17.
//

#ifndef LIBRATS_WINDOW_H
#define LIBRATS_WINDOW_H

#include <array>

namespace rats
{
    using WindowSize = std::array<unsigned int, 2>;

    class IWindow
    {
    public:

        virtual ~IWindow() = default;

        virtual void exitOnClose(bool exit) = 0;

        virtual void show() = 0;

        virtual void hide() = 0;

        virtual void close() = 0;

        virtual const WindowSize& size() const = 0;

    };
}

#endif //LIBRATS_WINDOW_H
