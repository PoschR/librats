//
// Created by robert on 16.02.17.
//

#ifndef LIBRATS_IGRAPHICSBUFFER_H
#define LIBRATS_IGRAPHICSBUFFER_H

namespace rats {

    enum class BufferType{
        STATIC,
        DYNAMIC
    };

    /**
     * Interface for a buffer that is stored on the graphics card.
     */
    class IGraphicsBuffer
    {
    public:

        virtual ~IGraphicsBuffer() = default;

        virtual void bind() = 0;

        virtual unsigned int size() const = 0 ;
    };
}


#endif //LIBRATS_IGRAPHICSBUFFER_H
