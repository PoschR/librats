//
// Created by robert on 01.08.17.
//

#include <string>
#include <stdexcept>

#include <glm/gtc/type_ptr.hpp>

#include <Game/Graphics/Shader.h>
#include <Game/Graphics/ShaderVariable.h>


namespace rats
{
    template<class ShaderType>
    ShaderVariable<ShaderType>::ShaderVariable(const std::string &name, const IShader &shader) :
            shader_(shader),
            name_(name)
    {
        shader_.bind();
        variable_id_ = glGetUniformLocation(shader_.id(), name_.c_str());
        if(variable_id_ < 0)
            throw std::invalid_argument("ShaderVariable: No variable named \""+name+"\" in current shader");
    }

    template<class ShaderType>
    void ShaderVariable<ShaderType>::operator=(const ShaderType &rhs) {
        throw std::invalid_argument("ShaderVariable: Type not supported!");
    }

    template<>
    void ShaderVariable<float>::operator=(const float& rhs) {
        glUniform1f(variable_id_, rhs);
    }

    template<>
    void ShaderVariable<glm::vec2>::operator=(const glm::vec2& rhs) {
        glUniform2f(variable_id_, rhs.x, rhs.y);
    }

    template<>
    void ShaderVariable<glm::vec3>::operator=(const glm::vec3 &rhs) {
        glUniform3f(variable_id_, rhs.x, rhs.y, rhs.z);
    }

    template<>
    void ShaderVariable<glm::mat4>::operator=(const glm::mat4 &rhs) {
        glUniformMatrix4fv(variable_id_,1,  GL_FALSE, glm::value_ptr(rhs));
    }

    template<>
    void ShaderVariable<Varying<float>>::operator=(const Varying<float> &rhs) {
        glUniform1fv(variable_id_, rhs.size(), rhs.data());
    }

    template<>
    void ShaderVariable<Varying<glm::vec2>>::operator=(const Varying<glm::vec2> &rhs) {
        glUniform2fv(variable_id_, rhs.size(), glm::value_ptr(rhs[0]));
    }

    template<>
    void ShaderVariable<Varying<glm::vec3>>::operator=(const Varying<glm::vec3> &rhs) {
        glUniform3fv(variable_id_, rhs.size(), glm::value_ptr(rhs[0]));
    }

    template<>
    void ShaderVariable<Varying<glm::mat4>>::operator=(const Varying<glm::mat4> &rhs) {
        glUniformMatrix4fv(variable_id_,1,  GL_FALSE, glm::value_ptr(rhs[0]));
    }


}
