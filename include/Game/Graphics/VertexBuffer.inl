
namespace rats
{
    template<class VertexSpecs>
    VertexBuffer<VertexSpecs>::VertexBuffer(BufferType type, const VertexLayout &layout,
                                            const std::vector<VertexSpecs> &vertices) :
    layout_(layout),
    vertices_(vertices)
    {
        type_ = type;

        glGenVertexArrays(1, &vertex_array_id_);
        glBindVertexArray(vertex_array_id_);


        glGenBuffers(1, &vertex_buffer_);
        glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer_);

        layout_.use();

        if (type_ == BufferType::STATIC)
            glBufferData(GL_ARRAY_BUFFER, vertices_.size() * sizeof(VertexSpecs), vertices_.data(), GL_STATIC_DRAW);
        else if (type_ == BufferType::DYNAMIC)
            glBufferData(GL_ARRAY_BUFFER, vertices_.size() * sizeof(VertexSpecs), vertices_.data(), GL_DYNAMIC_DRAW);
    }

    template<class VertexSpecs>
    VertexBuffer<VertexSpecs>::~VertexBuffer()
    {
        glDeleteBuffers(1, &vertex_buffer_);
        glDeleteVertexArrays(1, &vertex_array_id_);
    }


    template<class VertexSpecs>
    void VertexBuffer<VertexSpecs>::bind() {
        glBindVertexArray(vertex_array_id_);
    }

    template<class VertexSpecs>
    unsigned int VertexBuffer<VertexSpecs>::size() const {
        return (unsigned int) vertices_.size();
    }

}
