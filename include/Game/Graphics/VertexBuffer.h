//
// Created by robert on 16.02.17.
//

#ifndef LIBRATS_VERTEXBUFFER_H
#define LIBRATS_VERTEXBUFFER_H

#include <vector>
#include "GraphicsBuffer.h"
#include "VertexLayout.h"

#include <GL/glew.h>

namespace rats
{

    using VertexDimensions = std::vector<unsigned int>;

    template<class VertexSpecs>
    class VertexBuffer : public IGraphicsBuffer
    {
    public:

        VertexBuffer(BufferType type, const VertexLayout &layout, const std::vector<VertexSpecs> &vertices);

        virtual ~VertexBuffer();

        void bind();

        unsigned int size() const;

    private:
        BufferType type_;
        std::vector<VertexSpecs> vertices_;
        GLuint vertex_buffer_;
        GLuint vertex_array_id_;
        const VertexLayout& layout_;


    };
}

#include "VertexBuffer.inl"

#endif //LIBRATS_VERTEXBUFFER_H
