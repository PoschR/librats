//
// Created by robert on 10.02.17.
//

#ifndef LIBRATS_CAMERA_H
#define LIBRATS_CAMERA_H

#include <glm/mat4x4.hpp>
#include <glm/vec3.hpp>


namespace rats {

    using FoV = float;
    using Aspect = float;

    struct Frustum
    {
        FoV fov;
        Aspect aspect;
        float z_near;
        float z_far;

        Frustum(FoV fov, Aspect aspect, float z_near, float z_far) {
            this->fov = fov;
            this->aspect = aspect;
            this->z_near = z_near;
            this->z_far = z_far;
        }
    };

    using Position = glm::vec3;
    using LookAt = glm::vec3;
    using Direction = glm::vec3;
    using Translation = glm::vec3;

    class Camera
    {
    public:
        Camera(const Position& position, const LookAt& look_at, const Direction& up_vector, const Frustum& frustum);

        virtual ~Camera() = default;

        const glm::mat4& projection() const;

        const glm::mat4& view() const;

        const glm::mat4& viewProjection() const;

        void translate(const glm::vec3 &offset);

        const Position& position() const;

    private:
        Position position_;
        Direction up_vector_;
        LookAt look_at_;

        glm::mat4 projection_;
        glm::mat4 view_;
        glm::mat4 view_projection_;
    };


}



#endif //LIBRATS_CAMERA_H
