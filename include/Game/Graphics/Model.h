//
// Created by robert on 10.02.17.
//

#ifndef LIBRATS_MODEL_H
#define LIBRATS_MODEL_H

#include "Visual.h"

#include <array>
#include <memory>
#include <tuple>
#include <glm/mat4x4.hpp>
#include <glm/vec3.hpp>

#include <Game/GameObject.h>

#include "VertexBuffer.h"
#include "IndexBuffer.h"


namespace rats
{
    template <class VertexSpecs>
    using IndexBufferPointer = std::unique_ptr<IndexBuffer<VertexSpecs>>;
    using GraphicsBufferPointer = std::unique_ptr<IGraphicsBuffer>;
    using Vertices = std::vector<glm::vec3>;
    using Normals = std::vector<glm::vec3>;
    using Color = glm::vec3;

    const char* POSITION_ATTRIBUTE = "position";
    const char* NORMAL_ATTRIBUTE = "normal";
    const char* COLOR_ATTRIBUTE = "color";

    /**
     * A component which stores and displays geometry data. Handles the vertex or index buffer. Takes the transform
     * of the corresponding GameObject
     */
    class Model : public IVisual
    {
    public:
        /**
         * Construct a model with a given graphics buffer. Takes ownership of the graphics buffer.
         * @param graphics_buffer Unique pointer to the graphics buffer. Takes ownership of the buffer.
         * @param indexed Is the given buffer an index buffer or not?
         */
        Model(GraphicsBufferPointer graphics_buffer, bool indexed);

        virtual ~Model() = default;

        Model(Model&& model) = default;

        /**
         * Update internal transformation matrix to the one of the attached GameObject
         * @param game_object GameObject to which this Model is attached
         */
        void update(GameObject& game_object);

        /**
         * Binds the graphics buffer and draws it. Assumes that a right Shader is set.
         */
        void draw();

        /**
         * Return internal transformation matrix which is used for drawing
         * @return transformation matrix
         */
        const Transform& transform() const;

    private:
        bool indexed_ = false;
        Transform transform_;
        GraphicsBufferPointer graphics_buffer_;
    };

    /**
     * Construct positions and corresponding indices of a standard icosahedron
     * @return Tuple with vertices and indices of icosahedron
     */
    std::tuple<Vertices, Indices> make_icosahedron();

    /**
     * Construct positions and corresponding indices of a ico sphere with radius 1
     * @param refinement Indicates how many times the icosahedron is subdivided
     * @return Tuple of vertices and indices of ico sphere
     */
    std::tuple<Vertices, Indices> make_ico_sphere(const unsigned int refinement);

    /**
     * Generate a Model which can be used by the engine from the given vertices. Indices and normals are optional. Sets up the vertex buffer and
     * (if specified) the index buffer.
     * @tparam VertexSpecs Struct with vertex layout
     * @param vertices The vertices of the model
     * @param indices The indices of the model (can be empty)
     * @param normals The normals of the model (can be empty)
     * @param layout Layout information of VertexSpecs
     * @return The completed model which can be used as a component.
     */
    template<class VertexSpecs>
    Model make_model_from_data(const Vertices &vertices, const Indices &indices, const Normals &normals,
                               const VertexLayout &layout);

    /**
     * Generate a Model with a ico sphere which can be used as a component
     * @tparam VertexSpecs Struct with vertex layout
     * @param radius The radius of the sphere
     * @param layout Layout information of VertexSpecs
     * @param refinement How many times should the sphere be subdivided
     * @return The completed model with the sphere which can be used as a component.
     */
    template<class VertexSpecs>
    Model
    make_ico_sphere_model(const float radius, const unsigned int refinement, const VertexLayout &layout);

    template<class VertexSpecs>
    Model make_box_model(const Vec3& half_dims, const VertexLayout &layout);

    template<class VertexSpecs>
    Model make_plane_model(const Vec3& normal, const VertexLayout &layout);

}

#include "Model.inl"



#endif //LIBRATS_MODEL_H
