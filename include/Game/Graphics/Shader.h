//
// Created by robert on 01.08.17.
//

#ifndef RATS_SHADER_H
#define RATS_SHADER_H

#include <GL/glew.h>

namespace rats
{
    /**
     * Vertex- and pixel shader form a pipeline that is executed on the GPU. This interface specifies that a shader can
     * be bound in order to use it in a render pass as well as returning the id of the corresponding OpenGL program.
     * Subclasses are expected to specify their ShaderVariables, which can be used in render passes, etc.
     */
    class IShader
    {
    public:
        virtual ~IShader() = default;

        virtual void bind() const = 0;

        virtual const GLuint id() const = 0;
    };
}

#endif //RATS_SHADER_H
