//
// Created by robert on 10.02.17.
//

#ifndef LIBRATS_VISUALIZATION_H
#define LIBRATS_VISUALIZATION_H

#include <map>
#include <list>

namespace rats
{
    class IShader;
    class IVisual;
    class IWindow;
    class Camera;

    using VisualPointerList = std::list<IVisual*>;

    class Visualization {
    public:

        Visualization();

        virtual ~Visualization();


        void add(IVisual &visual, IShader &shader);

        void draw(const Camera& camera);


    private:
        std::map<IShader*, VisualPointerList*> render_passes_;
    };
}

#endif //LIBRATS_VISUALIZATION_H
