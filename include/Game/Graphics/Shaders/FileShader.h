//
// Created by robert on 10.02.17.
//

#ifndef LIBRATS_SHADER_H
#define LIBRATS_SHADER_H

#include <GL/glew.h>
#include <string>

#include "Game/Graphics/Shader.h"

namespace rats
{
    class VertexLayout;

    /**
     * Load a shader from file, compile and link it to a OpenGL program.
     */
    class FileShader : public IShader
    {
    public:

        FileShader(const std::string &vertex_shader, const std::string &fragment_shader);

        virtual ~FileShader();

        void bind() const;

        const GLuint id() const;

    private:
        GLuint shader_program_id_;

    };

}

#endif //LIBRATS_SHADER_H
