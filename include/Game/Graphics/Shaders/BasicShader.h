//
// Created by robert on 01.08.17.
//

#ifndef RATS_BASICSHADER_H
#define RATS_BASICSHADER_H

#include <GL/glew.h>

#include <glm/mat4x4.hpp>

#include <Game/Graphics/Shader.h>

namespace rats
{
    template<class ShaderType>
    class ShaderVariable;

    class BasicShader : public IShader
    {
    public:

        void bind() const override;

        const GLuint id() const override;

        const ShaderVariable<glm::mat4>& mvp() const;

    private:
        GLuint shader_id_;
    };
}

#endif //RATS_BASICSHADER_H
