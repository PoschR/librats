//
// Created by robert on 01.08.17.
//

#ifndef RATS_SHADERVARIABLE_H
#define RATS_SHADERVARIABLE_H

#include <string>
#include <vector>

#include <GL/glew.h>

#include <glm/vec3.hpp>
#include <glm/vec2.hpp>
#include <glm/mat4x4.hpp>


namespace rats
{
    class IShader;

    template<class T>
    using Varying = std::vector<T>;

    /**
     * ShaderVariable accesses uniform (normal or varying) in a shader code.
     * @tparam ShaderType Type of Uniform in shader code.
     */
    template<class ShaderType>
    class ShaderVariable
    {
    public:
        ShaderVariable(const std::string &name, const IShader &shader);

        virtual ~ShaderVariable() = default;

        void operator=(const ShaderType& rhs);

    private:
        const IShader& shader_;
        const std::string& name_;
        GLint variable_id_;
    };
}

#include "ShaderVariable.inl"

#endif //RATS_SHADERVARIABLE_H
