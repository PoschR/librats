//
// Created by robert on 10.02.17.
//

#ifndef LIBRATS_COMPONENT_H_H
#define LIBRATS_COMPONENT_H_H

#include <array>

namespace rats
{
    using Vec3 = std::array<float, 3>;

    class GameObject;

    /**
     * Component interface. GameObject uses this interface to specify its attached features.
     */
    class IComponent
    {
    public:

        virtual ~IComponent() = default;

        /**
         * Update method which is called once a frame.
         * @param gameObject Governing GameObject.
         */
        virtual void update(GameObject &gameObject) = 0;
    };
}

#endif //LIBRATS_COMPONENT_H_H
