//
// Created by robert on 10.02.17.
//

#ifndef LIBRATS_GAME_H
#define LIBRATS_GAME_H

#include <tuple>

namespace rats
{
    class Action;

    using Score = unsigned int;
    using Lives = unsigned int;

    /**
     *
     */
    class IGame
    {
    public:

        virtual ~IGame() = default;

        virtual void act(const Action& action) = 0;

        virtual void reset() = 0;

        virtual bool over() = 0;

        virtual Score score() = 0;

        virtual Lives lives() = 0;
    };
}

#endif //LIBRATS_GAME_H
