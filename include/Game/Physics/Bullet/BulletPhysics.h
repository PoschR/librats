//
// Created by robert on 10.02.17.
//

#ifndef LIBRATS_BULLETPHYSICS_H
#define LIBRATS_BULLETPHYSICS_H

#include <list>

#include "Game/Physics/PhysicalSystem.h"

class btDiscreteDynamicsWorld;

namespace rats
{
    class BulletBody;

    class BulletPhysics : public IRigidBodySystem
    {
    public:

        BulletPhysics();

        virtual ~BulletPhysics();

        IBody& add(const Shape &shape, const ShapeParameters &parameters, const Mass &mass, const Position &position) override;

        void step(float dt) override;

        void changeGravity(const Force &gravity) override;

    private:
        btDiscreteDynamicsWorld* world_;
        std::list<BulletBody*> bodies_;
    };
}

#endif //LIBRATS_BULLETPHYSICS_H
