//
// Created by robert on 10.02.17.
//

#ifndef LIBRATS_BULLETBODY_H
#define LIBRATS_BULLETBODY_H

#include "Game/Physics/Body.h"

class btCollisionShape;
class btMotionState;
class btRigidBody;

namespace rats
{
    class BulletBody : public IBody
    {
    public:

        BulletBody(const Shape &shape, const ShapeParameters &parameters, const Mass &mass, const Position &position);

        virtual ~BulletBody();

        void apply(const Impulse &impulse) override;

        void apply(const Impulse &impulse, const Position &position) override;

        Position position() override;

        Velocity velocity() override;

        void move(const Translation &offset) override;

        void moveTo(const Position &position) override;

        operator btRigidBody* ();

        void update(GameObject& gameObject);

    private:
        btCollisionShape* shape_;
        btMotionState* motion_state_;
        btRigidBody* rigid_body_;

    };
}

#endif //LIBRATS_BULLETBODY_H
