//
// Created by robert on 10.02.17.
//

#ifndef LIBRATS_BODY_H
#define LIBRATS_BODY_H

#include <vector>
#include <Game/Component.h>
#include <glm/vec3.hpp>

namespace rats
{
    using Mass = float;
    using ShapeParameters = std::vector<float>;
    using Force = glm::vec3;
    using Impulse = glm::vec3;
    using Position = glm::vec3;
    using Velocity = glm::vec3;
    using Translation = glm::vec3;

    enum class Shape {
        SPHERE,
        BOX,
        PLANE
    };

    /**
     * Simple rigid body component, which the engine uses.
     */
    class IBody : public IComponent
    {
    public:

        virtual ~IBody() = default;

        virtual void apply(const Impulse& impulse) = 0;

        virtual void apply(const Impulse& impulse, const Position& position) = 0;

        virtual Position position() = 0;

        virtual Velocity velocity() = 0;

        virtual void move(const Translation &offset) = 0;

        virtual void moveTo(const Position& position) = 0;

        virtual void update(GameObject& gameObject) = 0;
    };
}

#endif //LIBRATS_BODY_H
