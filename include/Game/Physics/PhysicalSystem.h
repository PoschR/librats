//
// Created by robert on 10.02.17.
//

#ifndef LIBRATS_PHYSICALSYSTEM_H
#define LIBRATS_PHYSICALSYSTEM_H

#include "Body.h"


namespace rats
{
    class IRigidBodySystem
    {
    public:

        virtual ~IRigidBodySystem() = default;

        virtual IBody& add(const Shape &shape, const ShapeParameters &parameters, const Mass &mass, const Position &position) = 0;

        virtual void step(float dt) = 0;

        virtual void changeGravity(const Force &gravity) = 0;
    };
}

#endif //LIBRATS_PHYSICALSYSTEM_H
