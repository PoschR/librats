//
// Created by robert on 28.11.16.
//

#ifndef LIBRATS_ENVIRONMENT_H
#define LIBRATS_ENVIRONMENT_H

#include <vector>

namespace rats
{
    using Screen = std::vector<float>;
    using Reward = float;
    using Terminated = bool;
    using Observation = std::tuple<Reward, Terminated, Screen*>;

    class ActionSet;
    class Action;

    /**
     *
     */
    enum class EnvironmentMode
    {
        TRAINING,
        TESTING,
        PLAYING
    };

    class IEnvironment
    {
    public:
        virtual ~IEnvironment() = default;

        virtual Observation act(const Action& action) = 0;

        virtual const ActionSet& usedActionSet() const = 0;

        virtual void restart() = 0;

        virtual void changeMode(const EnvironmentMode& mode) = 0;
    };
}

#endif //LIBRATS_ENVIRONMENT_H
