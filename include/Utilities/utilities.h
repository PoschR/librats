//
// Created by robert on 04.07.17.
//

#ifndef LIBRATS_UTILITIES_H_H
#define LIBRATS_UTILITIES_H_H

#include <utility>
#include <memory>
#include <string>

template<typename T, typename ...Args>
std::unique_ptr<T> make_unique( Args&& ...args ) noexcept
{
    return std::unique_ptr<T>( new T( std::forward<Args>(args)... ) );
}

size_t gl_type_size(GLenum type);

std::string check_gl_errors();

#endif //LIBRATS_UTILITIES_H_H
