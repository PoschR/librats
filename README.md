# RATS (Reinforcement Agent Training for Simulations)

The RATS library offers an environment for constructing simulations in which an agent can
be trainined using Deep Q-learning. Currently the focus is on games using rigid-body systems, but there are plans for
more general systems as well.
