//
// Created by robert on 28.11.16.
//

#include "../include/Game/GameObject.h"
#include "Game/Component.h"

namespace rats {

    GameObject::GameObject() {
        components_ = {};
    }


    GameObject::GameObject(const glm::mat4 &transform) {
        components_ = {};
        transform_ = transform;
    }

    GameObject::GameObject(std::initializer_list<IComponent *> components) {
        components_ = components;
    }

    GameObject::GameObject(const std::list<IComponent*> &components, const glm::mat4 &transform) {
        components_ = components;
        transform_ = transform;
    }

    GameObject::~GameObject() {
        //TODO take care of ownership!
        /*for(auto* component : components_)
            delete component;*/
    }

    void GameObject::addComponent(IComponent &component) {
        components_.push_back(&component);
    }

    void GameObject::update()
    {
        for(auto* component : components_)
            component->update(*this);
    }

}