//
// Created by robert on 10.02.17.
//

#include "Game/Physics/Bullet/BulletBody.h"

#include <btBulletDynamicsCommon.h>
#include <Game/GameObject.h>
#include <glm/gtc/type_ptr.hpp>

namespace rats {

    btCollisionShape *createBulletShape(const Shape &shape, const ShapeParameters &parameters)
    {
        switch (shape) {
            case Shape::SPHERE:
                assert(parameters.size() == 1);
                return new btSphereShape(parameters[0]);

            case Shape::BOX:
                assert(parameters.size() == 3);
                return new btBoxShape(btVector3(parameters[0], parameters[1], parameters[2]));

            case Shape::PLANE:
                assert(parameters.size() == 4);
                return new btStaticPlaneShape(btVector3(parameters[0], parameters[1], parameters[2]), parameters[3]);
        }
        return nullptr;
    }

    BulletBody::BulletBody(const Shape &shape, const ShapeParameters &parameters, const Mass &mass, const Position &position)
    {
        shape_ = createBulletShape(shape, parameters);
        assert(shape_ != nullptr);

        btVector3 pos(position[0], position[1], position[2]);
        motion_state_ = new btDefaultMotionState(btTransform(btQuaternion(0, 0, 0, 1), pos));

        btVector3 inertia(0, 0, 0);
        shape_->calculateLocalInertia(mass, inertia);

        rigid_body_ = new btRigidBody(mass, motion_state_, shape_, inertia);
    }

    BulletBody::~BulletBody() {
        delete shape_;
        delete motion_state_;
        delete rigid_body_;
    }

    void BulletBody::apply(const Impulse &impulse)
    {
        btVector3 central_impulse(impulse[0], impulse[1], impulse[2]);

        rigid_body_->activate(true);
        rigid_body_->applyCentralImpulse(central_impulse);
    }

    void BulletBody::apply(const Impulse &impulse, const Position &position)
    {
        btVector3 offset_impulse(impulse[0], impulse[1], impulse[2]);
        btVector3 offset_position(position[0], position[1], position[2]);

        rigid_body_->activate(true);
        rigid_body_->applyImpulse(offset_impulse, offset_position);
    }

    Position BulletBody::position()
    {
        btVector3 &origin = rigid_body_->getWorldTransform().getOrigin();
        return Position{origin.x(), origin.y(), origin.z()};
    }

    Velocity BulletBody::velocity()
    {
        const btVector3 &velocity = rigid_body_->getLinearVelocity();
        return Velocity{velocity.x(), velocity.y(), velocity.z()};
    }

    void BulletBody::move(const Translation &offset)
    {
        btVector3 bt_offset(offset[0], offset[1], offset[2]);
        rigid_body_->translate(bt_offset);
    }

    void BulletBody::moveTo(const Position &position)
    {
        btTransform new_position(btQuaternion(0, 0, 0, 1), btVector3(position[0], position[1], position[2]));
        rigid_body_->setWorldTransform(new_position);
    }

    BulletBody::operator btRigidBody* () {
        return rigid_body_;
    }

    void BulletBody::update(GameObject &gameObject)
    {
        btTransform& transform = rigid_body_->getWorldTransform();
        transform.getOpenGLMatrix(glm::value_ptr(gameObject.transform_));
    }

}

