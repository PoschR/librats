//
// Created by robert on 10.02.17.
//

#include "Game/Physics/Bullet/BulletPhysics.h"
#include "Game/Physics/Bullet/BulletBody.h"

#include <btBulletDynamicsCommon.h>

namespace rats {

    BulletPhysics::BulletPhysics()
    {
        btCollisionConfiguration* collisionConfiguration = new btDefaultCollisionConfiguration();

        world_ = new btDiscreteDynamicsWorld(
                new btCollisionDispatcher(collisionConfiguration),
                new btDbvtBroadphase(),
                new btSequentialImpulseConstraintSolver,
                collisionConfiguration
        );
    }

    BulletPhysics::~BulletPhysics()
    {
        delete world_;
        for(auto* body : bodies_)
            delete body;
    }

    IBody &BulletPhysics::add(const Shape &shape, const ShapeParameters &parameters, const Mass &mass, const Position &position)
    {
        BulletBody* new_body = new BulletBody(shape, parameters, mass, position);
        world_->addRigidBody(*new_body);

        bodies_.push_back(new_body);

        return *new_body;
    }

    void BulletPhysics::step(float dt) {
        world_->stepSimulation(dt);
    }

    void BulletPhysics::changeGravity(const Force &gravity)
    {
        btVector3 bt_gravity(gravity[0], gravity[1], gravity[2]);
        world_->setGravity(bt_gravity);
    }
}