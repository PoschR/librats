//
// Created by robert on 14.07.17.
//

#include <iostream>
#include <sstream>

#include <GL/glew.h>


size_t gl_type_size(GLenum type)
{
    switch(type) {
        case GL_HALF_FLOAT:
            return sizeof(GLhalf);
        case GL_FLOAT:
            return sizeof(GLfloat);
        case GL_DOUBLE:
            return sizeof(GLdouble);
        case GL_FIXED:
            return sizeof(GLfixed);
        case GL_BYTE:
            return sizeof(GLbyte);
        case GL_UNSIGNED_BYTE:
            return sizeof(GLubyte);
        case GL_SHORT:
            return sizeof(GLshort);
        case GL_UNSIGNED_SHORT:
            return sizeof(GLushort);
        case GL_INT:
            return sizeof(GLint);
        case GL_UNSIGNED_INT:
            return sizeof(GLuint);
        default:
            return 0;
    }
}

std::string check_gl_errors()
{
    std::stringstream err_stream;

    int i=0;
    for(GLenum err; (err=glGetError()) != GL_NO_ERROR;)
    {
        if(i > 0)
            err_stream <<", ";

        switch(err) {
            case GL_INVALID_ENUM:
                err_stream << "INVALID_ENUM";
                break;
            case GL_INVALID_VALUE:
                err_stream << "INVALID_VALUE";
                break;
            case GL_INVALID_OPERATION:
                err_stream << "INVALID_OPERATION";
                break;
            case GL_INVALID_FRAMEBUFFER_OPERATION:
                err_stream << "INVALID_FRAMEBUFFER_OPERATION";
            case GL_OUT_OF_MEMORY:
                err_stream << "OUT_OF_MEMORY";
                break;
            case GL_STACK_UNDERFLOW:
                err_stream << "STACK_UNDERFLOW";
                break;
            case GL_STACK_OVERFLOW:
                err_stream << "STACK_OVERFLOW";
                break;
        }

        i++;
    }

    std::stringstream final_string;
    if(i == 1)
        final_string << i << " OpenGL error (" << err_stream.str() << ") raised!";
    else if(i > 1)
        final_string << i << " OpenGL errors (" << err_stream.str() <<") raised!";

    return final_string.str();
}