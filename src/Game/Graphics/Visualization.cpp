//
// Created by robert on 10.02.17.
//

#include "../../../include/Game/Graphics/Visualization.h"


#include <GL/glew.h>

#include <Game/Graphics/Camera.h>
#include <Game/Graphics/Shader.h>
#include <Game/Graphics/ShaderVariable.h>
#include <Game/Graphics/Visual.h>
#include <Game/Graphics/Window.h>



namespace rats {

    Visualization::Visualization()
    {
        glewExperimental = GL_TRUE;
        if(glewInit() != GLEW_OK)
            throw std::runtime_error("Visualization: couldn't initialize glew!");

        //turn on depth testing
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LESS);

        //turn on clock counter-wise culling
        glEnable(GL_CULL_FACE);
        glFrontFace(GL_CCW);
    }

    Visualization::~Visualization()
    {
        for(auto& pass : render_passes_)
            delete pass.second;
    }

    void Visualization::add(IVisual &visual, IShader &shader)
    {
        if(render_passes_.empty())
            render_passes_.emplace(std::pair<IShader*, VisualPointerList*>(&shader, new VisualPointerList{&visual}));

        auto pair = render_passes_.find(&shader);
        if(pair == std::end(render_passes_))
            render_passes_.emplace(std::pair<IShader*, VisualPointerList*>(&shader, new VisualPointerList{&visual}));
        else
            pair->second->push_back(&visual);
    }

    void Visualization::draw(const Camera &camera)
    {
        glm::mat4 view_projection = camera.viewProjection();

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        for(auto& pass : render_passes_)
        {
            pass.first->bind();
            for(auto& visual : *pass.second)
            {
                ShaderVariable<glm::mat4> mvp("MVP", *pass.first);
                mvp = view_projection * visual->transform();
                visual->draw();
            }
        }
    }


}