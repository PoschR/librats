//
// Created by robert on 10.02.17.
//

#include "../../../include/Game/Graphics/Camera.h"

#include <glm/gtx/transform.hpp>

rats::Camera::Camera(const Position& position, const LookAt& look_at, const Direction& up_vector, const rats::Frustum &frustum)
{
    position_ = position;
    up_vector_ = up_vector;
    look_at_ = look_at;

    view_ = glm::lookAt(position_, look_at_, up_vector_);
    projection_ = glm::perspective(frustum.fov, frustum.aspect, frustum.z_near, frustum.z_far);

    view_projection_ = projection_ * view_ ;
}

const glm::mat4 &rats::Camera::projection() const {
    return projection_;
}

const glm::mat4 &rats::Camera::view() const {
    return view_;
}

const glm::mat4 &rats::Camera::viewProjection() const {
    return view_projection_;
}

void rats::Camera::translate(const glm::vec3 &offset)
{
    position_ += offset;
    look_at_ += offset;

    view_ = glm::lookAt(position_, look_at_, up_vector_);
    view_projection_ = projection_ * view_;
}

const rats::Position& rats::Camera::position() const {
    return position_;
}
