//
// Created by robert on 14.07.17.
//

#include <Game/Graphics/VertexLayout.h>

#include <Utilities/utilities.h>
#include <assert.h>

namespace rats{

    VertexAttribute::VertexAttribute(const std::string &name, size_t offset, size_t size, size_t stride, GLenum type) :
        name_(name),
        offset_(offset),
        size_(size),
        type_(type),
        stride_(stride)
    {}

    size_t VertexAttribute::offset() const {
        return offset_;
    }

    size_t VertexAttribute::size() const {
        return size_;
    }

    GLenum VertexAttribute::type() const {
        return type_;
    }

    size_t VertexAttribute::stride() const {
        return stride_;
    }

    const std::string &VertexAttribute::name() const {
        return name_;
    }

    VertexLayout::VertexLayout(std::initializer_list<VertexAttribute> vertex_attributes)  :
            attributes_(vertex_attributes)
    {

    }

    void VertexLayout::use() const
    {
        for(int i=0; i<attributes_.size(); i++)
            this->useAttribute(i);
    }

    void VertexLayout::useAttribute(unsigned int attribute_index) const
    {
        assert(attribute_index < attributes_.size());

        auto& attribute = attributes_[attribute_index];

        glEnableVertexAttribArray(attribute_index);
        glVertexAttribPointer(attribute_index, attribute.size(), attribute.type(), GL_FALSE, attribute.stride(),
                              (void *) attribute.offset());
    }


    const ConstAttributeIterator VertexLayout::find(const std::string& attribute_name) const
    {
        for(auto it = std::begin(attributes_); it != std::end(attributes_); it++)
        {
            if(it->name().compare(attribute_name) == 0)
                return it;
        }

        return std::end(attributes_);
    }

    const ConstAttributeIterator VertexLayout::end() const  {
        return std::end(attributes_);
    }

}