//
// Created by robert on 10.02.17.
//

#include "Game/Graphics/Shaders/FileShader.h"

#include <iostream>
#include <fstream>
#include <sstream>

#include <Game/Graphics/VertexLayout.h>
#include <Utilities/utilities.h>



namespace rats{

    std::string file_to_string(const char *filename);

    void compile_shader(GLuint shaderID, const char *shader_code);

    GLuint load_shaders(const std::string &vertex_shader_file, const std::string &fragment_shader_file);

    FileShader::FileShader(const std::string &vertex_shader, const std::string &fragment_shader)
    {
        shader_program_id_ = load_shaders(vertex_shader, fragment_shader);
    }

    FileShader::~FileShader() {
        glDeleteProgram(shader_program_id_);
    }

    GLuint load_shaders(const std::string &vertex_shader_file, const std::string &fragment_shader_file)
    {
        //Create shader objects on GPU
        GLuint vertex_shader_id = glCreateShader(GL_VERTEX_SHADER);
        GLuint fragment_shader_id = glCreateShader(GL_FRAGMENT_SHADER);

        //copy shader code to string
        std::string vertex_shader_code = file_to_string(vertex_shader_file.c_str());
        std::string fragment_shader_code = file_to_string(fragment_shader_file.c_str());

        std::cout << "Compiling vertex shader: " << vertex_shader_file << std::endl;
        compile_shader(vertex_shader_id, vertex_shader_code.c_str());

        std::cout << "Compiling fragment shader: " << fragment_shader_file << std::endl;
        compile_shader(fragment_shader_id, fragment_shader_code.c_str());

        std::cout << "Linking shader program" << std::endl;
        GLuint shader_program_id = glCreateProgram();
        glAttachShader(shader_program_id, vertex_shader_id);
        glAttachShader(shader_program_id, fragment_shader_id);
        glLinkProgram(shader_program_id);

        //check program
        GLint result = GL_FALSE;
        int infoLogLevel;
        glGetProgramiv(shader_program_id, GL_LINK_STATUS, &result);
        glGetProgramiv(shader_program_id, GL_INFO_LOG_LENGTH, &infoLogLevel);
        if(infoLogLevel > 0)
        {
            char errorMessage[infoLogLevel+1];
            glGetProgramInfoLog(shader_program_id, infoLogLevel, NULL, errorMessage);
            std::cout << errorMessage << std::endl;
        }

        glDetachShader(shader_program_id, vertex_shader_id);
        glDetachShader(shader_program_id, fragment_shader_id);

        glDeleteShader(vertex_shader_id);
        glDeleteShader(fragment_shader_id);

        return shader_program_id;
    }

    void FileShader::bind() const
    {
        glUseProgram(shader_program_id_);
    }

    const GLuint FileShader::id() const{
        return shader_program_id_;
    }

    void compile_shader(GLuint shaderID, const char *shader_code)
    {
        GLint result = GL_FALSE;
        int infoLogLevel;

        //compile shader
        glShaderSource(shaderID, 1, &shader_code, NULL);
        glCompileShader(shaderID);

        //check shader
        glGetShaderiv(shaderID, GL_COMPILE_STATUS, &result);
        glGetShaderiv(shaderID, GL_INFO_LOG_LENGTH, &infoLogLevel);
        if(infoLogLevel > 0)
        {
            char errorMessage[infoLogLevel+1];
            glGetShaderInfoLog(shaderID, infoLogLevel, NULL, errorMessage);
            std::cerr << errorMessage << std::endl;
        }
    }

    std::string file_to_string(const char *filename)
    {
        std::string text;
        std::ifstream fileStream(filename, std::ios::in);

        if(!fileStream.is_open()){
            std::cerr << "Couldn't open file: " << filename << std::endl;
            return text;
        }

        //load file to string
        std::string line = "";
        while(getline(fileStream, line))
            text += "\n" + line;

        fileStream.close();

        return text;
    }
}